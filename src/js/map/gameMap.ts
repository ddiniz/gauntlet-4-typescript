import Sprite from "../renderer/sprite";
import { Coord, CoordAndDimensions } from "../utils";
import { AUTOTILE_FLOOR_MAP, AUTOTILE_WALL_MAP } from "./autotileMaps";
import Tile, { TileType } from "./tile";

export default class GameMap {
  public simpleMap: any = null;
  public map: Tile[][] = [];
  public mapWidth: number = 0;
  public mapHeigth: number = 0;
  public scale: number = 2;
  public scaledSpriteSize: number = 16 * this.scale;

  constructor(mapJson: any) {
    this.simpleMap = mapJson;
    this.map = this.generateAutotiledMap(this.simpleMap);
  }

  private initializeAutotiledMap(): any[][] {
    let newMap: Tile[][] = [];
    for (let i = 0; i < this.mapWidth; i++) {
      newMap.push([]);
      for (let j = 0; j < this.mapHeigth; j++) {
        newMap[i].push(new Tile(TileType.FLOOR));
      }
    }
    return newMap;
  }

  private generateAutotiledMap(simpleMap: any[][]): any[][] {
    if (this.mapWidth != simpleMap.length) this.mapWidth = simpleMap.length;
    if (this.mapHeigth != simpleMap[0].length)
      this.mapHeigth = simpleMap.length;

    let newAutotiledMap: Tile[][] = this.initializeAutotiledMap();

    let wallKeys = {};
    let floorKeys = {};
    for (let i = 0; i < simpleMap.length; i++) {
      for (let j = 0; j < simpleMap[i].length; j++) {
        let tileData = this.decideTileSprite(simpleMap, i, j);
        newAutotiledMap[i][j].tileType = tileData.type;
        newAutotiledMap[i][j].tileSpriteNumber = tileData.autoTileNumber;
        newAutotiledMap[i][j].sprite = new Sprite(
          new Coord(j * this.scaledSpriteSize, i * this.scaledSpriteSize),
          new CoordAndDimensions(tileData.x, tileData.y, 16, 16),
          (tileData.type === TileType.FLOOR ? "floor" : "wall") +
            tileData.value,
          this.scale
        );
        let key = "x" + tileData.x + "y" + tileData.y;
        if (tileData.type === TileType.WALL) (wallKeys as any)[key] = "";
        else (floorKeys as any)[key] = "";
      }
    }
    let numwall = Object.keys(wallKeys).length;
    let numfloor = Object.keys(floorKeys).length;
    console.log(numwall + " " + numfloor);
    return newAutotiledMap;
  }

  changeMapTile(mouseCoord: Coord, tileType: number) {
    if (
      mouseCoord.x >= 0 &&
      mouseCoord.x < this.simpleMap.length &&
      mouseCoord.y >= 0 &&
      mouseCoord.y < this.simpleMap[mouseCoord.x].length
    ) {
      this.simpleMap[mouseCoord.x][mouseCoord.y] = tileType;
      this.map = this.generateAutotiledMap(this.simpleMap);
    }
  }

  private isWall(val: number) {
    return val < 0 ? 1 : 0;
  }

  private bitwiseTileOperation(map: any, x: number, y: number): number {
    let nw = x > 0 && y > 0 ? this.isWall(map[x - 1][y - 1]) : 1;
    let _n = y > 0 ? this.isWall(map[x + 0][y - 1]) : 1;
    let ne = x < map.length - 1 && y > 0 ? this.isWall(map[x + 1][y - 1]) : 1;
    let _w = x > 0 ? this.isWall(map[x - 1][y + 0]) : 1;
    let _e = x < map.length - 1 ? this.isWall(map[x + 1][y + 0]) : 1;
    let sw =
      x > 0 && y < map[x].length - 1 ? this.isWall(map[x - 1][y + 1]) : 1;
    let _s = y < map[x].length - 1 ? this.isWall(map[x + 0][y + 1]) : 1;
    let se =
      x < map.length - 1 && y < map[x].length - 1
        ? this.isWall(map[x + 1][y + 1])
        : 1;
    let tileSpriteNumber = 0;
    tileSpriteNumber |= nw;
    tileSpriteNumber |= _n * 2;
    tileSpriteNumber |= ne * 4;
    tileSpriteNumber |= _w * 8;
    tileSpriteNumber |= _e * 16;
    tileSpriteNumber |= sw * 32;
    tileSpriteNumber |= _s * 64;
    tileSpriteNumber |= se * 128;
    return tileSpriteNumber;
  }

  private decideTileSprite(map: any, x: number, y: number): any {
    if (!map) return;
    let tileSpriteNumber = this.bitwiseTileOperation(map, x, y);
    let tileData = null;
    let val = map[x][y];
    if (this.isWall(val)) {
      tileData = (AUTOTILE_WALL_MAP as any)[tileSpriteNumber];
      tileData.type = TileType.WALL;
      tileData.value = -val;
    } else {
      tileData = (AUTOTILE_FLOOR_MAP as any)[tileSpriteNumber];
      tileData.type = TileType.FLOOR;
      tileData.value = val;
    }
    tileData.autoTileNumber = tileSpriteNumber;
    return tileData;
  }

  public generateTestTileMap(num: number): void {
    this.simpleMap = [];
    this.simpleMap.push([-1, -1, -1, -1, -1]);
    this.simpleMap.push([-1, -1, -1, -1, -1]);

    let nw = (num & 1) == 1 ? -1 : 1;
    let _n = (num & 2) == 2 ? -1 : 1;
    let ne = (num & 4) == 4 ? -1 : 1;
    let _w = (num & 8) == 8 ? -1 : 1;
    let _e = (num & 16) == 16 ? -1 : 1;
    let sw = (num & 32) == 32 ? -1 : 1;
    let _s = (num & 64) == 64 ? -1 : 1;
    let se = (num & 128) == 128 ? -1 : 1;

    this.simpleMap.push([-1, nw, _n, ne, -1]);
    this.simpleMap.push([-1, _w, -1, _e, -1]);
    this.simpleMap.push([-1, sw, _s, se, -1]);
    this.simpleMap.push([-1, -1, -1, -1, -1]);
    this.map = this.generateAutotiledMap(this.simpleMap);
  }

  public importMapStart(document: Document): void {
    let that = this;
    document.getElementById("import").onchange = function (ev: Event) {
      that.importMap(ev);
    };
    document.getElementById("import").click();
  }

  private importMap(ev: Event) {
    let fr = new FileReader();
    debugger;
    let that = this;
    fr.onload = function (e) {
      let result = JSON.parse(e.target.result as string);
      that.simpleMap = JSON.stringify(result, null, 2);
    };
  }

  public exportMap(document: Document, simpleMap: any): void {
    const a = document.createElement("a");
    a.href = URL.createObjectURL(
      new Blob([JSON.stringify(simpleMap, null, 2)], {
        type: "text/plain",
      })
    );
    a.setAttribute("download", "data.txt");
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  public render(ctx: CanvasRenderingContext2D): void {
    for (let i = 0; i < this.map.length; i++) {
      for (let j = 0; j < this.map[i].length; j++) {
        this.map[i][j].sprite.render(ctx);
      }
    }
  }
}
