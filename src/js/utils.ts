export class Coord {
  public x: number;
  public y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

export class CoordAndDimensions {
  public x: number;
  public y: number;
  public w: number;
  public h: number;
  constructor(x: number, y: number, w: number, h: number) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  inBounds(coord: Coord): boolean {
    return (
      this.x <= coord.x && //
      coord.x <= this.x + this.w && //
      this.y <= coord.y && //
      coord.y <= this.y + this.h //
    );
  }
}
