import Sprite from "../renderer/sprite";
import { Coord, CoordAndDimensions } from "../utils";
import Widget from "./widget";

var buttonIdGlobal = -1;

export class Button extends Widget {
  public id: number;
  public action: Function;
  public disabled: boolean;

  constructor(action: Function = null) {
    super();
    this.action = action;
  }

  private collisionTest(mouseCoord: Coord): boolean {
    return this.coord.inBounds(mouseCoord);
  }
  public tryActionIfPressed(mouseCoord: Coord): boolean {
    if (!this.disabled && this.collisionTest(mouseCoord)) {
      this.action();
      return true;
    } else {
      return false;
    }
  }

  public render(ctx: CanvasRenderingContext2D): void {
    throw new Error("Method not implemented.");
  }
}

export class TextButton extends Button {
  public text: string;
  public font: string;
  constructor(
    text: string,
    font: string,
    x: number,
    y: number,
    w: number,
    h: number,
    action: Function = null
  ) {
    super(action);
    this.font = font;
    this.id = buttonIdGlobal++;
    this.text = text;
    this.coord = new CoordAndDimensions(x, y, w, h);
  }

  public render(ctx: CanvasRenderingContext2D): void {
    ctx.font = this.font;
    ctx.strokeRect(
      this.coord.x - this.coord.w / 2,
      this.coord.y - this.coord.h / 2,
      this.coord.w,
      this.coord.h
    );
    ctx.textAlign = "center";
    ctx.fillText(this.text, this.coord.x, this.coord.y);
  }
}

export class ImageButton extends Button {
  public sprite: Sprite;

  constructor(
    sprite: Sprite,
    x: number,
    y: number,
    w: number,
    h: number,
    action: Function = null
  ) {
    super(action);
    this.id = buttonIdGlobal++;
    this.sprite = sprite;
    this.coord = new CoordAndDimensions(x, y, w, h);
  }

  public render(ctx: CanvasRenderingContext2D): void {
    ctx.strokeRect(this.coord.x - 1, this.coord.y - 1, 33, 33);
    this.sprite.render(ctx);
  }
}
