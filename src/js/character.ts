import Sprite from "./renderer/sprite";
import { Coord } from "./utils";

export default class Character {
  public pos: Coord;
  public movespeed: number;
  public health: number;
  public state: CharacterState;
  public meleeDmg: number;
  public rangedDmg: number;
  public acceleration: number;
  public fricction: number;
  public sprite: Sprite;
}

enum CharacterState {
  IDLE,
  WALKING,
  ATTACKING,
  DEAD,
}

export class Projectile {
  public pos: Coord;
  public movespeed: number;
  public sprite: Sprite;
}
