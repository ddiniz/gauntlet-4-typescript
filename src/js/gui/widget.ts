import { CoordAndDimensions } from "../utils";

export default abstract class Widget {
  public coord: CoordAndDimensions;

  public abstract render(ctx: CanvasRenderingContext2D): void;


}
