import { Coord } from "../utils";
import PlayerKeyboardInput, {
  checkAllKeyboardKeysStates,
  KeyboardInput,
  PlayerMouseInput,
} from "./playerInput";
export class InputSingleton {
  public keyboard: PlayerKeyboardInput = new PlayerKeyboardInput();
  public mouse: PlayerMouseInput = new PlayerMouseInput();

  constructor() {
    console.log("oi");
  }

  public initialize(doc: Document, canvas: HTMLCanvasElement): void {
    this.addKeypressEventListener(doc, canvas);
  }

  public getMouseCoord(): Coord {
    return this.mouse.coord;
  }
  public getMouseTileCoord(): Coord {
    return this.mouse.tileCoord;
  }

  public handleKeyAndMousePressStates(): void {
    for (let inputName in this.keyboard) {
      let inp: KeyboardInput = (this.keyboard as any)[inputName];
      inp.runCurrentState();
    }
    this.mouse.leftBtn.runCurrentState();
    this.mouse.rightBtn.runCurrentState();
    this.mouse.middleBtn.runCurrentState();
  }

  private addKeypressEventListener(
    doc: Document,
    canvas: HTMLCanvasElement
  ): void {
    doc.addEventListener("keydown", (e) => {
      checkAllKeyboardKeysStates(this.keyboard, e, true);
    });
    doc.addEventListener("keyup", (e) => {
      checkAllKeyboardKeysStates(this.keyboard, e, false);
    });
    canvas.addEventListener("mousedown", (ev) => {
      if (ev.button === 0) this.mouse.leftBtn.keyDown = true;
      else if (ev.button === 2) this.mouse.rightBtn.keyDown = true;
    });
    canvas.addEventListener("mouseup", (ev) => {
      if (ev.button === 0) this.mouse.leftBtn.keyDown = false;
      else if (ev.button === 2) this.mouse.rightBtn.keyDown = false;
    });
    canvas.addEventListener("mousemove", (ev) => {
      this.mouse.coord.x = ev.clientX;
      this.mouse.coord.y = ev.clientY;
      this.mouse.tileCoord.x = Math.floor(ev.clientY / 32);
      this.mouse.tileCoord.y = Math.floor(ev.clientX / 32);
    });
  }
}

var input = new InputSingleton();
export default input;
