import mapJson from "../../assets/maps/testmap.json";
import Character from "../character";
import Gui from "../gui/gui";
import input from "../input/InputSingleton";
import GameMap from "../map/gameMap";
import spriteSheet from "../renderer/spriteSheets";
import { Coord } from "../utils";
import Scene from "./scene";

export default class BattleScene extends Scene {
  private gui: Gui = null;
  private character: Character = null;
  private gameMap: GameMap = null;

  constructor(canvas: HTMLCanvasElement, changeSceneCallBack: Function) {
    super(canvas, changeSceneCallBack);
    this.gameMap = this.gameMap = new GameMap(mapJson);
    this.gui = new Gui();
    this.character = new Character();
    this.initializeGuiElements();
  }

  private getVerticalDirectionValue(): number {
    let up = input.keyboard.up.isFirstPressOrHolding();
    let down = input.keyboard.down.isFirstPressOrHolding();
    if (up && !down) return 1;
    if (!up && down) return -1;
    return 0;
  }
  private getHorizontalDirectionValue(): number {
    let left = input.keyboard.left.isFirstPressOrHolding();
    let right = input.keyboard.right.isFirstPressOrHolding();
    if (left && !right) return 1;
    if (!left && right) return -1;
    return 0;
  }

  public handleInput(): void {
    let melee = input.keyboard.melee.isFirstPressOrHolding();
    let ranged = input.keyboard.ranged.isFirstPressOrHolding();
    let directionVector = new Coord(
      this.getVerticalDirectionValue(),
      this.getHorizontalDirectionValue()
    );
    if (melee || ranged) {
      //attack
    } else {
      //
    }
  }

  public handleGameStates(): void {}
  public handlePhysics(): void {}

  public handleGraphics(ctx: CanvasRenderingContext2D): void {
    if (!spriteSheet.canDraw) return;
    this.gameMap.render(ctx);
    this.gui.render(ctx);
  }

  private initializeGuiElements(): void {}
}
