import input from "./input/InputSingleton";
import Renderer from "./renderer/renderer";
import spriteSheet from "./renderer/spriteSheets";
import BattleScene from "./scenes/battleScene";
import MainMenuScene from "./scenes/mainMenuScene";
import MapEditorScene from "./scenes/mapEditorScene";
import Scene, { GameScenes } from "./scenes/scene";

export default class Game {
  private canvas: HTMLCanvasElement;
  private framerate: number = 16.66;
  private renderer: Renderer = null;

  private currentScene: Scene = null;

  constructor() {
    this.canvas = this.initializeCanvas(document);
    const ctx = this.initializeContext(this.canvas);

    this.renderer = new Renderer(ctx);
    this.removeCanvasRightClickContextMenu(this.canvas);
    input.initialize(document, this.canvas);
    this.changeScene(GameScenes.MAIN_MENU);
    setInterval(this.gameLoopInterval, this.framerate);
  }

  private changeScene = (newScene: GameScenes) => {
    switch (newScene) {
      case GameScenes.MAIN_MENU:
        this.currentScene = new MainMenuScene(this.canvas, this.changeScene);
        break;
      case GameScenes.MAP_EDITOR:
        this.currentScene = new MapEditorScene(this.canvas, this.changeScene);
        break;
      case GameScenes.BATTLE:
        this.currentScene = new BattleScene(this.canvas, this.changeScene);
        break;
    }
  };

  private gameLoopInterval = () => {
    this.handleKeyPresses();
    this.handleGameStates();
    this.handlePhysics();
    this.handleGraphics();
  };

  private initializeCanvas(document: Document): HTMLCanvasElement {
    let canvas: HTMLCanvasElement = <HTMLCanvasElement>(
      document.getElementById("canvas")
    );
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    return canvas;
  }

  private initializeContext(
    canvas: HTMLCanvasElement
  ): CanvasRenderingContext2D {
    let context: CanvasRenderingContext2D = canvas.getContext("2d");
    // this.context.webkitImageSmoothingEnabled = false;
    // this.context.mozImageSmoothingEnabled = false;
    context.imageSmoothingEnabled = false;
    return context;
  }

  private removeCanvasRightClickContextMenu(canvas: HTMLCanvasElement) {
    canvas.oncontextmenu = function (e) {
      e.preventDefault();
    };
  }

  public handleKeyPresses(): void {
    input.handleKeyAndMousePressStates();
    this.currentScene.handleInput();
  }

  public handleGameStates(): void {
    this.currentScene.handleGameStates();
  }
  public handlePhysics(): void {
    this.currentScene.handlePhysics();
  }

  public handleGraphics(): void {
    if (!spriteSheet.canDraw) return;
    this.renderer.clear(this.canvas.width, this.canvas.height);
    this.currentScene.handleGraphics(this.renderer.ctx);
  }
}
